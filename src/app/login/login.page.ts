import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  loginResult: any;

  constructor(
    private router:Router
  ) { 
    this.loginResult = "Kullanıcı bilgilerini giriniz";
  }

  ngOnInit() {
  }

  checkUser() {
    this.router.navigate(["menu/home"])
  }

}
